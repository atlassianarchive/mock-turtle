package com.atlassian.intsys.mockturtle;

import javax.servlet.Servlet;

/**
 * Copyright Atlassian: 1/11/12
 */
public interface ServicesServletContainer extends Servlet
{
    String getContext();
    void setContext(String ctx);

    ServicesContainer getServices();
    void setServices(ServicesContainer services);
}
