package com.atlassian.intsys.mockturtle;

import java.util.List;

/**
 * Container to merge contents of other containers. Mainly of use when using multiple containers..
 *
 * Copyright Atlassian: 4/1/13
 */
public class AggregateServicesContainer extends AbstractServicesContainer
{
    public AggregateServicesContainer(List<ServicesContainer> containers)
    {
        super();
        for (ServicesContainer container: containers) {
            for (Class c: container.getServiceClasses()) {
                services.put(c, container.getService(c));
            }
        }
    }

    public void setServices(List<Class> interfaces)
    {
        throw new UnsupportedOperationException();
    }

}
