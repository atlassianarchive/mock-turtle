package com.atlassian.intsys.mockturtle.mockito;

import com.atlassian.intsys.mockturtle.AbstractServicesContainer;

import java.util.List;

import static org.mockito.Mockito.mock;

/**
 * Copyright Atlassian: 1/11/12
 */
public class MockitoServicesContainer extends AbstractServicesContainer
{

    public void setServices(List<Class> interfaces)
    {
        // Generate Mockito implementations
        for (Class i : interfaces) {
            services.put(i, mock(i));
        }
    }

}
