package com.atlassian.intsys.mockturtle.mockito;

import com.atlassian.intsys.mockturtle.AbstractServicesContainer;
import com.atlassian.intsys.mockturtle.DelegateContainer;

import java.lang.reflect.Field;
import java.util.*;

import static org.mockito.Mockito.mock;

/**
 * Copyright Atlassian: 1/11/12
 */
public class DelegatingMockitoServiceContainer extends AbstractServicesContainer implements DelegateContainer
{

    private List<Object> delegators = new ArrayList<Object>();

    public void setServices(List<Class> interfaces)
    {
        // Generate mockito implementations
        for (Class i : interfaces) {
            Object from;
            try {
                from = i.newInstance();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            Object mock = mock(i);

            Class service = delegate(i, from, mock);

            services.put(service, mock);
            delegators.add(from);
        }
    }

    // Injects a mock into a REST delegating class
    private Class delegate(Class klass, Object from, Object to) {
        try {

            Class[] ifaces = klass.getInterfaces();
            if (ifaces.length != 1)
                throw new RuntimeException("Only expected one interface for class "+klass.getSimpleName());

            Class service = ifaces[0];

            for (Field f: klass.getDeclaredFields()) {
                if (f.getType().equals(service)) {
                    f.setAccessible(true);
                    f.set(from, to);
                    return service;
                }
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        throw new IllegalStateException("Couldn't find delegating field for class "+klass.getSimpleName());
    }

    public List<Object> getDelegators()
    {
        return delegators;
    }

}
