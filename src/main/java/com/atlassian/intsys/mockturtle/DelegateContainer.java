package com.atlassian.intsys.mockturtle;

import java.util.List;

/**
 * Copyright Atlassian: 1/11/12
 */
public interface DelegateContainer
{
    List<Object> getDelegators();
}
