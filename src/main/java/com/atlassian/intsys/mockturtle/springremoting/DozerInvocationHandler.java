package com.atlassian.intsys.mockturtle.springremoting;

import org.dozer.DozerBeanMapper;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class DozerInvocationHandler implements InvocationHandler {
    private Object impl;
    private DozerBeanMapper dozerBeanMapper;

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        args = convertList(args);

        try {
            Object result = method.invoke(impl, args);
            result = convert(result);
            return result;
        } catch (InvocationTargetException ex) {
            throw ex.getCause();
        }
    }

    protected Object[] convertList(Object[] args) {
        if (args == null) {
            return null;
        }
        Object[] result = new Object[args.length];
        for (int i = 0; i < args.length; ++i) {
            result[i] = convert(args[i]);
        }
        return result;
    }

    protected Object convert(Object obj) {
        if (obj == null) {
            return null;
        }

        if (obj instanceof List) {
            return convertListWithDozer(castToList(obj));
        }

        return mapWithDozer(obj);
    }

    private Object convertListWithDozer(List<Object> inputList) {
        List<Object> result = new ArrayList<Object>();
        for (Object o : inputList) {
            result.add(mapWithDozer(o));
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    private List<Object> castToList(Object o) {
        return (List)o;
    }

    private Object mapWithDozer(Object obj) {
        if (obj.getClass().getName().indexOf('$') < 0) {
            return obj;
        }

        return dozerBeanMapper.map(obj, obj.getClass().getSuperclass());
    }

    public void setImpl(Object impl) {
        this.impl = impl;
    }

    public void setDozerBeanMapper(DozerBeanMapper dozerBeanMapper) {
        this.dozerBeanMapper = dozerBeanMapper;
    }
}
