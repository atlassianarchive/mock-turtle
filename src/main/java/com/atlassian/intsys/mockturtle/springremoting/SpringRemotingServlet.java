package com.atlassian.intsys.mockturtle.springremoting;

import com.atlassian.intsys.mockturtle.ServicesContainer;
import com.atlassian.intsys.mockturtle.ServicesServletContainer;
import org.dozer.DozerBeanMapper;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.remoting.httpinvoker.HttpInvokerServiceExporter;
import org.springframework.util.StringUtils;
import org.springframework.web.HttpRequestMethodNotSupportedException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright Atlassian: 4/01/13
 */
public class SpringRemotingServlet extends HttpServlet implements ServicesServletContainer
{
    @Resource
    private DozerBeanMapper dozerBeanMapper;

    private String context;
    private ServicesContainer services;
    private Map<String, HttpInvokerServiceExporter> exporters = new HashMap<String, HttpInvokerServiceExporter>();

    @Override
    public void init() throws ServletException
    {
        for (Class<?> service : services.getServiceClasses()) {
            exportService(service);
        }
    }

    protected void exportService(Class<?> service)
    {
        Object mockService = services.getService(service);
        mockService = wrapWithDozerProxy(service, mockService);

        HttpInvokerServiceExporter exporter = new HttpInvokerServiceExporter();
        exporter.setServiceInterface(service);
        exporter.setService(mockService);

        exporter.afterPropertiesSet();
        exporters.put(service.getSimpleName(), exporter);
    }

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        LocaleContextHolder.setLocale(request.getLocale());
        try {
            HttpInvokerServiceExporter exporter = findExporter(request.getPathInfo().substring(1));
            if (exporter != null) {
                exporter.handleRequest(request, response);
            } else {
                response.sendError(HttpServletResponse.SC_NOT_FOUND);
            }
        }
        catch (HttpRequestMethodNotSupportedException ex) {
            String[] supportedMethods = ex.getSupportedMethods();
            if (supportedMethods != null) {
                response.setHeader("Allow", StringUtils.arrayToDelimitedString(supportedMethods, ", "));
            }
            response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, ex.getMessage());
        }
        finally {
            LocaleContextHolder.resetLocaleContext();
        }
    }

    protected HttpInvokerServiceExporter findExporter(String path)
    {
        for (String name : exporters.keySet()) {
            if (path.startsWith(name)) {
                return exporters.get(name);
            }
        }
        return null;
    }

    protected Object wrapWithDozerProxy(Class<?> service, Object impl)
    {
        DozerInvocationHandler handler = new DozerInvocationHandler();
        handler.setImpl(impl);
        handler.setDozerBeanMapper(dozerBeanMapper);
        return Proxy.newProxyInstance(
            service.getClassLoader(),
            new Class[]{service},
            handler
        );
    }

    public ServicesContainer getServices()
    {
        return services;
    }

    public void setServices(ServicesContainer services)
    {
        this.services = services;
    }

    public String getContext()
    {
        return context;
    }

    public void setContext(String context)
    {
        this.context = context;
    }

    public void setDozerBeanMapper(DozerBeanMapper dozerBeanMapper)
    {
        this.dozerBeanMapper = dozerBeanMapper;
    }
}
