package com.atlassian.intsys.mockturtle;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.mockito.Mockito.reset;

/**
 * Copyright Atlassian: 1/11/12
 */
public abstract class AbstractServicesContainer implements ServicesContainer
{
    protected Map<Class, Object> services;

    protected AbstractServicesContainer()
    {
        services = new HashMap<Class, Object>();
    }

    protected AbstractServicesContainer(Map<Class, Object> services)
    {
        this.services = services;
    }

    // Convenience wrapper for single-service containers
    public void setService(Class klass) {
        setServices(Collections.singletonList(klass));
    }

    @SuppressWarnings("unchecked")
    public <T> T getService(Class<T> klass)
    {
        if (!services.containsKey(klass)) {
            throw new IllegalArgumentException("Service hasn't been added: " + klass);
        }
        return (T)services.get(klass);
    }

    public Set<Class> getServiceClasses() {
        return services.keySet();
    }

    public void resetServices()
    {
        for (Object service: services.values()) {
            reset(service);
        }
    }
}
