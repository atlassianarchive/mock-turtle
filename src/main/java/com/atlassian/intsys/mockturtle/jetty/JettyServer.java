package com.atlassian.intsys.mockturtle.jetty;

import com.atlassian.intsys.mockturtle.ServicesServletContainer;
import com.atlassian.intsys.mockturtle.MockServer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

import static org.mockito.Mockito.mock;

/**
 * Copyright Atlassian: 30/10/12
 */
public class JettyServer implements MockServer
{
    private Server server;
    private List<ServicesServletContainer> servlets;
    private int port;

    public void start()
    {
        server = new Server(port);

        ServletContextHandler contextHandler = new ServletContextHandler(ServletContextHandler.SESSIONS);
        contextHandler.setContextPath("/");
        server.setHandler(contextHandler);

        for (ServicesServletContainer servlet: servlets) {
            String context = servlet.getContext();
            if (context == null)
                context = "";
            else if (context != "" && !context.startsWith("/"))
                context = "/"+context;

            contextHandler.addServlet(new ServletHolder(servlet), context);
        }

        try{
            server.start();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void stop()
    {
        try{
            server.stop();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Required
    public void setServlets(List<ServicesServletContainer> servlets)
    {
        this.servlets = servlets;
    }

    @Required
    public void setPort(int port)
    {
        this.port = port;
    }
}
