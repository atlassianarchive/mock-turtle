package com.atlassian.intsys.mockturtle;

import java.util.List;

/**
 * Copyright Atlassian: 1/11/12
 */
public interface MockServer
{
    void setPort(int port);
    void setServlets(List<ServicesServletContainer> servlets);

    void start();
    void stop();
}
