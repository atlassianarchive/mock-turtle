package com.atlassian.intsys.mockturtle.cxf;

import com.atlassian.intsys.mockturtle.ServicesContainer;
import com.atlassian.intsys.mockturtle.ServicesServletContainer;
import org.apache.cxf.Bus;
import org.apache.cxf.BusFactory;
import org.apache.cxf.jaxws.JaxWsServerFactoryBean;
import org.apache.cxf.transport.servlet.CXFNonSpringServlet;

import javax.servlet.ServletConfig;

/**
 * Copyright Atlassian: 1/11/12
 */
public class CxfServicesServlet extends CXFNonSpringServlet implements ServicesServletContainer
{

    // Delegate as we need to inherit from CXF
    private ServicesContainer services;

    public ServicesContainer getServices()
    {
        return services;
    }

    public void setServices(ServicesContainer services)
    {
        this.services = services;
    }


    private String context;

    public String getContext()
    {
        return context;
    }

    public void setContext(String ctx)
    {
        this.context = ctx;
    }


    @Override
    // Called at startup time to register this web service.
    public void loadBus(ServletConfig servletConfig) {
        super.loadBus(servletConfig);

        Bus bus = getBus();
        BusFactory.setDefaultBus(bus);

        for (Class klass : services.getServiceClasses()) {
            String name = decamelName(klass);
            String addr = "/"+name;

            JaxWsServerFactoryBean fb = new JaxWsServerFactoryBean();
            fb.setServiceBean(services.getService(klass));
            fb.setServiceClass(klass);
            fb.setAddress(addr);
            fb.create();
        }
    }

    private String decamelName(Class klass) {
        String camel = klass.getSimpleName();
        return Character.toLowerCase(camel.charAt(0))
               + (camel.length() > 1 ? camel.substring(1) : "");
    }

}
