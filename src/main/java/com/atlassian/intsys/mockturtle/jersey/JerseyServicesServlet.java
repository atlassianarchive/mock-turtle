package com.atlassian.intsys.mockturtle.jersey;

import com.atlassian.intsys.mockturtle.DelegateContainer;
import com.atlassian.intsys.mockturtle.ServicesContainer;
import com.atlassian.intsys.mockturtle.ServicesServletContainer;
import com.sun.jersey.api.core.DefaultResourceConfig;
import com.sun.jersey.spi.container.servlet.ServletContainer;

/**
 * Copyright Atlassian: 1/11/12
 */
public class JerseyServicesServlet extends ServletContainer implements ServicesServletContainer
{
    private ServicesContainer services;
    private String context;

    public static class ServiceResourceConfig extends DefaultResourceConfig {
        public ServiceResourceConfig(DelegateContainer services)
        {
            getSingletons().addAll(services.getDelegators());
        }
    }

    public JerseyServicesServlet(ServicesContainer services)
    {
        super(new ServiceResourceConfig((DelegateContainer)services));
        this.services = services;
    }

    public ServicesContainer getServices()
    {
        return services;
    }

    public void setServices(ServicesContainer services)
    {
        this.services = services;
    }

    public String getContext()
    {
        return context;
    }

    public void setContext(String context)
    {
        this.context = context;
    }
}
