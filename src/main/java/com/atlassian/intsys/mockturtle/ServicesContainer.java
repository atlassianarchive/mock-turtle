package com.atlassian.intsys.mockturtle;

import java.util.List;
import java.util.Set;

/**
 * Copyright Atlassian: 1/11/12
 */
public interface ServicesContainer
{
    void setServices(List<Class> interfaces);

    <T> T getService(Class<T> klass);

    Set<Class> getServiceClasses();

    void resetServices();
}
