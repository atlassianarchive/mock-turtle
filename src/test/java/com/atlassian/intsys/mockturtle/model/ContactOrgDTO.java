package com.atlassian.intsys.mockturtle.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Copyright Atlassian: 26/09/12
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ContactOrgDTO implements Serializable
{
    private static final long serialVersionUID = -4369894690955417530L;

    @XmlID
    private String id;

    private String firstName;
    private String lastName;
    private String email;
    private String phone;

    private String screenName;
    private String department;
    private String position;
    private String homepageUrl;
    private String location;
    private String aboutMe;
    private String avatarType;
    private String avatarUrl;
    private String instantMessenger;

    // Read-only, derived from Contact data
    private String displayName;
    private Long vendorId;
    private boolean mainContact;
    private String netSuiteId;


    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public Long getVendorId()
    {
        return vendorId;
    }

    public boolean isMainContact()
    {
        return mainContact;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setVendorId(Long vendorId) {
        this.vendorId = vendorId;
    }

    public void setMainContact(boolean mainContact) {
        this.mainContact = mainContact;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getHomepageUrl() {
        return homepageUrl;
    }

    public void setHomepageUrl(String homepageUrl) {
        this.homepageUrl = homepageUrl;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getInstantMessenger() {
        return instantMessenger;
    }

    public void setInstantMessenger(String instantMessenger) {
        this.instantMessenger = instantMessenger;
    }

    public String getAvatarType() {
        return avatarType;
    }

    public void setAvatarType(String avatarType) {
        this.avatarType = avatarType;
    }

    public String getNetSuiteId() {
        return netSuiteId;
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

}
