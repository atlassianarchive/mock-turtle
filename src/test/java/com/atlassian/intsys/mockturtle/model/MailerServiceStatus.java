package com.atlassian.intsys.mockturtle.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
* Copyright Atlassian: 02/08/2010
*/
@XmlRootElement(name = "status")
public enum MailerServiceStatus
{
    OK,
    UNAVAILABLE
}
