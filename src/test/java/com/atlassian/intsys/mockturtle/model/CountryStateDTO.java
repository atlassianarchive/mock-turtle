package com.atlassian.intsys.mockturtle.model;

import java.io.Serializable;

public class CountryStateDTO implements Serializable
{
    private static final long serialVersionUID = -8810479477538518405L;

    private String countryIsoCode;
    private String stateName;

    public String getCountryIsoCode() {
        return countryIsoCode;
    }

    public void setCountryIsoCode(String countryIsoCode) {
        this.countryIsoCode = countryIsoCode;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }
}
