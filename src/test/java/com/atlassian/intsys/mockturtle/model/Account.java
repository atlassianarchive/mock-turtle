package com.atlassian.intsys.mockturtle.model;

import javax.xml.bind.annotation.*;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Account
{

    private Long id;
    private String name;

    @XmlID  // When we have custom sequences we need to make the IDs unique in the XML namespace
    public String getXmlID()
    {
        return "ACCID:"+id;
    }
    public void setXmlID(String xmlID)
    {
        // Ignore
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
