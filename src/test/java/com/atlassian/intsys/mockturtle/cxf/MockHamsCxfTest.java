package com.atlassian.intsys.mockturtle.cxf;

import com.atlassian.intsys.mockturtle.ServicesContainer;
import com.atlassian.intsys.mockturtle.model.Account;
import com.atlassian.intsys.mockturtle.services.AccountManagementService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Copyright Atlassian: 30/10/12
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/mockHamsCxf-ctx.xml"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class MockHamsCxfTest
{
    @Resource ServicesContainer cxfService;

    @Resource AccountManagementService accountManagementServiceClient;

    @Before
    public void setup() throws Exception {
        assertNotNull(cxfService);
        assertNotNull(accountManagementServiceClient);

        cxfService.resetServices();
    }

    @After
    public void teardown() throws Exception {
        cxfService.resetServices();
    }

    @Test
    public void testLegacySOAPService() throws Exception
    {
        Account account = new Account();
        account.setName(UUID.randomUUID().toString());

        AccountManagementService service = cxfService.getService(AccountManagementService.class);
        when(service.getAccountById(anyLong())).thenReturn(account);

        Account newAccount = accountManagementServiceClient.getAccountById(99L);
        assertNotNull(newAccount);
        assertEquals(account.getName(), newAccount.getName());

    }
}
