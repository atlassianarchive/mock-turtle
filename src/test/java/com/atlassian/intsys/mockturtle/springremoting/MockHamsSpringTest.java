package com.atlassian.intsys.mockturtle.springremoting;

import com.atlassian.intsys.mockturtle.ServicesContainer;
import com.atlassian.intsys.mockturtle.model.ContactOrgDTO;
import com.atlassian.intsys.mockturtle.model.CountryStateDTO;
import com.atlassian.intsys.mockturtle.services.ExternalContactService;
import com.atlassian.intsys.mockturtle.services.ExternalCountryService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Copyright Atlassian: 30/10/12
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/mockHamsSpringRemoting-ctx.xml"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class MockHamsSpringTest
{
    @Resource ServicesContainer springRemoteServices;

    @Resource ExternalContactService externalContactServiceClient;
    @Resource ExternalCountryService externalCountryServiceClient;

    @Before
    public void setup() throws Exception {
        assertNotNull(springRemoteServices);
        assertNotNull(externalContactServiceClient);

        springRemoteServices.resetServices();
    }

    @After
    public void teardown() throws Exception {
        springRemoteServices.resetServices();
    }

    @Test
    public void testSimpleHamsCallback() throws Exception
    {
        String dummy = UUID.randomUUID().toString();
        ContactOrgDTO dto = new ContactOrgDTO();
        dto.setEmail(dummy);

        ExternalContactService service = springRemoteServices.getService(ExternalContactService.class);
        when(service.getContactByEmail(anyString())).thenReturn(dto);

        ContactOrgDTO contact = externalContactServiceClient.getContactByEmail(dummy);
        assertNotNull(contact);
        assertEquals(dummy, contact.getEmail());

        verify(service).getContactByEmail(dummy);
        verifyZeroInteractions(service);
    }

    @Test
    public void testSimpleHamsCallback_argsSpecified() throws Exception
    {
        String dummy = UUID.randomUUID().toString();

        ExternalContactService svc = springRemoteServices.getService(ExternalContactService.class);
        ContactOrgDTO dto = new ContactOrgDTO();
        dto.setEmail(dummy);

        when(svc.getContactByEmail(dummy)).thenReturn(dto);

        ContactOrgDTO contact = externalContactServiceClient.getContactByEmail(dummy);
        assertNotNull(contact);
        assertEquals(dummy, contact.getEmail());

        contact = externalContactServiceClient.getContactByEmail("wibble");
        assertNull(contact);
    }

    @Test
    public void testCountryService() throws Exception
    {
        ExternalCountryService service = springRemoteServices.getService(ExternalCountryService.class);

        CountryStateDTO dto = new CountryStateDTO();
        dto.setCountryIsoCode("ISO");
        dto.setStateName("Iso");

        when(service.getCountryState(anyString(), anyString())).thenReturn(dto);

        CountryStateDTO ret = externalCountryServiceClient.getCountryState("wibble", "wobble");
        assertNotNull(ret);
        assertEquals(ret.getStateName(), dto.getStateName());

    }

    @Test
    public void testReset() throws Exception
    {
        String dummy = UUID.randomUUID().toString();
        ContactOrgDTO dto = new ContactOrgDTO();
        dto.setEmail(dummy);

        ExternalContactService svc = springRemoteServices.getService(ExternalContactService.class);
        when(svc.getContactByEmail(anyString())).thenReturn(dto);

        ContactOrgDTO contact = externalContactServiceClient.getContactByEmail(dummy);
        assertNotNull(contact);
        assertEquals(dummy, contact.getEmail());

        // Call should still return contact
        contact = externalContactServiceClient.getContactByEmail(dummy);
        assertNotNull(contact);
        assertEquals(dummy, contact.getEmail());


        springRemoteServices.resetServices();

        // Should return null after reset
        contact = externalContactServiceClient.getContactByEmail(dummy);
        assertNull(contact);
    }

    @Test
    public void testSerializeInlineClassesInResult()
    {
        ContactOrgDTO dodgyContact = new ContactOrgDTO(){{
            setFirstName("John");
            setLastName("Smith");
            setEmail("foobar@example.com");
        }};

        ExternalContactService svc = springRemoteServices.getService(ExternalContactService.class);
        when(svc.getContactByEmail("foobar@example.com")).thenReturn(dodgyContact);

        ContactOrgDTO result = externalContactServiceClient.getContactByEmail("foobar@example.com");

        assertNotNull(result);
        assertEquals(dodgyContact.getFirstName(), result.getFirstName());
        assertEquals(dodgyContact.getLastName(), result.getLastName());
        assertEquals(dodgyContact.getEmail(), result.getEmail());
    }

    @Test
    public void testSerializeInlineClassesInArguments()
    {
        ContactOrgDTO dodgyContact = new ContactOrgDTO(){{
            setEmail("foobar@example.com");
        }};

        ContactOrgDTO safeContact = new ContactOrgDTO();
        safeContact.setEmail("foobar@example.com");

        assertEquals(dodgyContact, safeContact);

        ExternalContactService svc = springRemoteServices.getService(ExternalContactService.class);
        when(svc.saveContact(dodgyContact)).thenReturn(dodgyContact);

        ContactOrgDTO result = externalContactServiceClient.saveContact(safeContact);

        assertNotNull(result);
        assertEquals(dodgyContact.getEmail(), result.getEmail());
    }

    @Test
    public void testMockServiceThrowsException()
    {
        ContactOrgDTO safeContact = new ContactOrgDTO();
        safeContact.setEmail("foobar@example.com");

        ExternalContactService svc = springRemoteServices.getService(ExternalContactService.class);
        when(svc.saveContact(any(ContactOrgDTO.class))).thenThrow(new IllegalArgumentException());

        try {
            ContactOrgDTO result = externalContactServiceClient.saveContact(safeContact);
            fail("expected exception");
        } catch (Exception e) {
            assertThat(e, is(instanceOf(IllegalArgumentException.class)));
        }
    }
}
