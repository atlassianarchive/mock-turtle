package com.atlassian.intsys.mockturtle.services;

import com.atlassian.intsys.mockturtle.model.Account;

import javax.jws.WebService;

/**
 * Copyright Atlassian: 16/09/11
 */
@WebService
public interface AccountManagementService
{

    Account getAccountById(Long id);

}
