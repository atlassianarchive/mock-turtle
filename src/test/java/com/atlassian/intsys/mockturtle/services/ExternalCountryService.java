package com.atlassian.intsys.mockturtle.services;


import com.atlassian.intsys.mockturtle.model.CountryStateDTO;

import javax.jws.WebService;

@WebService
public interface ExternalCountryService
{

    public CountryStateDTO getCountryState(String countryIsoCode, String stateName);
}
