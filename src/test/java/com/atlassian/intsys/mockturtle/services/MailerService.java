package com.atlassian.intsys.mockturtle.services;


import com.atlassian.intsys.mockturtle.model.MailerServiceStatus;

/**
 * Copyright Atlassian: 28/07/2010
 */
public interface MailerService
{
    public static final String VERSION = "1.0";
    public static final String PATH = "/mailerservice/";
    public static final String STATUS_PATH = "/status";


    /**
     * Get overall mailer status
     */
    MailerServiceStatus getStatus();

}
