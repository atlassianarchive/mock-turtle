package com.atlassian.intsys.mockturtle.services;

import com.atlassian.intsys.mockturtle.model.MailerServiceStatus;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Copyright Atlassian: 02/08/2010
 */

@Path(MailerService.PATH + MailerService.VERSION)
@Produces({MediaType.APPLICATION_XML})
@Consumes({MediaType.APPLICATION_XML})
public class MailerServiceRestEndpoint implements MailerService
{
    private final Log log = LogFactory.getLog(this.getClass());

    private MailerService mailProviderLocal;    

    @GET
    @Path(STATUS_PATH)
    public MailerServiceStatus getStatus()
    {
        return mailProviderLocal.getStatus();
    }
}
