package com.atlassian.intsys.mockturtle.services;


import com.atlassian.intsys.mockturtle.model.ContactOrgDTO;

import javax.jws.WebService;

/**
 * Contact and organisation operations, using DTOs.
 *
 * Copyright Atlassian: 3/02/12
 */
@WebService
public interface ExternalContactService
{
    ContactOrgDTO getContactByEmail(String email);

    ContactOrgDTO saveContact(ContactOrgDTO contact);

}
