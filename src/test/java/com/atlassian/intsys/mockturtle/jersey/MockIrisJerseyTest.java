package com.atlassian.intsys.mockturtle.jersey;

import com.atlassian.intsys.mockturtle.ServicesContainer;
import com.atlassian.intsys.mockturtle.model.MailerServiceStatus;
import com.atlassian.intsys.mockturtle.services.MailerService;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

/**
 * Copyright Atlassian: 30/10/12
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/mockIrisJersey-ctx.xml"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class MockIrisJerseyTest
{
    @Resource ServicesContainer jerseyService;
    @Resource String irisServerBase;
    private WebResource resourceClient;

    @Before
    public void setup() throws Exception {
        assertNotNull(jerseyService);

        ClientConfig cc = new DefaultClientConfig();
        cc.getFeatures().put(ClientConfig.FEATURE_DISABLE_XML_SECURITY, true);

        resourceClient = Client.create(cc)
            .resource(irisServerBase);

        jerseyService.resetServices();
    }

    @After
    public void teardown() throws Exception {
        jerseyService.resetServices();
    }

    @Test
    public void testStatusService() throws Exception
    {
        MailerService service = jerseyService.getService(MailerService.class);

        when(service.getStatus()).thenReturn(MailerServiceStatus.OK);

        MailerServiceStatus status = resourceClient
            .path("/mailerservice/1.0/status")
            .get(MailerServiceStatus.class);

        assertNotNull(status);
        assertEquals(MailerServiceStatus.OK, status);
    }

}
