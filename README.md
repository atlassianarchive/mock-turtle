# Mock Turtle

## What it is

Mock Turtle is simple framework for creating mock servers for internal
systems services. What this means is that we can create network
accessible versions of any internal-systems service from within an
integration test case and drive its responses via Mockito. Currently
it can mock Jersey, Spring HTTP Remoting and CXF services.

## Why it is

Integration tests should treat the target system as a black-box,
focusing on its inputs and outputs, including it's interactions with
other services. For external services a common method of validating
outputs was by stubbing external services, logging from them and then
parsing the logs. But stubs are not a great solution; you tend to have
to use magic inputs to elicit the desired behaviour (e.g. to have a
credit-card transaction fail you have a magic CC number that the stub
treats specially).

Mock Turtle is a framework for creating mock network services that we
can dynamically control from within our tests with the simplicity of
mocking in unit tests.

## How it works

![Architecture diagram](https://bitbucket.org/atlassian/mock-turtle/raw/5ba49975c02fc097cde9db364ff8427a88dea0f3/docs/diagram.png)

Mock Turtle works by embedding one or more Jetty servers into the
integration test (usually via a Spring Test context). These servers
are configured to serve a CXF or Jersey servlet under whatever context
the production servers are configured for. Each server is primed to
instantiate one or more services (e.g. the HAMS
ExternalContactService); however rather than being wired up to a
concrete implementation, a Mockito mock object is generated for the
service. This mock object is made available to the integration test,
which now controls what the service returns, via the usual Mockito
when, etc wrappers. The server being tested (i.e. Hamlet in the above
illustration) is configured (via the 'inttest' profile) to talk to
these mock services, closing the loop. Using Mock Turtle

The following is based on the integration tests; consult the source
there for more detailed usage examples. These examples use Spring Test
to create the services, however calling these APIs programmatically is
possible too.

## Mocking CXF services

CXF services are mocked on the interfaces; this assumes the JAX-WS
annotations are on the service interfaces.

The first thing to do is create a CXF servlet to respond to HTTP
requests on the required service interfaces. A special CXF servlet
that generates Mockito objects is already defined. You'll need to
inject this into your tests, so give it a meaningful name:

    <bean id="mockHamsService" class="com.atlassian.intsys.mockturtle.cxf.CxfMockitoServlet">
        <property name="services">
            <list>
                <value>com.atlassian.hams.external.service.ExternalContactService</value>
                <value>com.atlassian.hams.external.service.ExternalCountryService</value>
                <value>com.atlassian.hams.external.service.ExternalStatusService</value>
            </list>
        </property>
    </bean>

Next we'll need a Jetty server to house the servlet. We don't need to
interact with the server once it's created so it may not be injected,
so we must mark it as non-lazy so it starts up:

    <bean id="mockHamsServer" class="com.atlassian.intsys.mockturtle.jetty.JettyServer"
          init-method="start" destroy-method="stop"
          lazy-init="false">
        <property name="port" value="${hams.server.port}"/>
        <property name="services">
            <list><ref bean="mockHamsService"/></list>
        </property>
    </bean>

Note that ${hams.server.port} must be set to whatever the inttest
profile in the application being tested uses.

Next we need our test instance. This will be setup to use the Spring
Test context and inject the service container (i.e. mockHamsService):

    @RunWith(SpringJUnit4ClassRunner.class)
    @ContextConfiguration(locations = {"/TestBase-ctx.xml"})
    public class StatusITCase
    {
        @Resource ServiceContainer mockHamsService;

        @Before
        public void setUp() throws Exception
        {
            mockHamsService.resetServices();
        }

        @After
        public void tearDown() throws Exception
        {
            mockHamsService.resetServices();
        }

        @Test
        public void testStatusChecksHams() throws Exception
        {
            // Fetch the mock service and set it to return true for status calls
            ExternalStatusService service = mockHamsService.getService(ExternalStatusService.class);
            when(statusService.isHamsAvailable()).thenReturn(true);

            // Call the service
            String status = hamletClient
                .path(StatusService.RESOURCE)
                .get(String.class);

            // Sanity checks
            assertNotNull(status);
            assertEquals("OK", status);

            // Standard mockito verification
            verify(service).isHamsAvailable();
            verifyNoMoreInteractions(service);
        }
    }

## Mocking Jersey services

Mocking Jersey services works in a similar manner, with one proviso...

Ideally we would define the REST API annotations on the interface and
mock them the same way as CXF. However, it is not recommended to place
root-resource definitions (i.e. `@Path("/myservce/.."))` on interfaces
as when we produce concrete implementations of these Jersey gets sees
two root definitions (the interface and the inherited `@Path` on the
class). Consequently the Jersey mocking-servlet is designed to use the
concrete class as the target. It extracts the interface from that (via
reflection), instantiates a concrete instance, generates a mock,
injects the mock into the instance and feeds that to the servlet.

The upshot of all this is that:

 * Your Jersey annotations should be on a pure delegating class, that merely passes the calls through to a local implementation (i.e. *Local.java). Luckily this is how most of our Jersey services are implemented.

 * These delegating Jersey classes must be separated out into their own module, so that they can be added as a dependency to integration test modules of anything that depends on the service. Currently our REST services are embedded into the server WAR module; however splitting these out isn't too hard, see Iris or Hamlet for examples of doing this.

Once these prerequisites have been met you can mock the service. This
is basically the same as with CXF, but we pass the Jersey-annotated
classes to the servlet instead:

    <bean id="mockIrisService" class="com.atlassian.intsys.mockturtle.jersey.JerseyMockitoServlet">
         <constructor-arg>
             <bean class="com.atlassian.intsys.mockturtle.mockito.DelegatingMockitoServiceContainer">
                 <property name="services">
                     <list>
                         <value>com.atlassian.iris.service.rest.MailerServiceRestEndpoint</value>
                     </list>
                 </property>
             </bean>
         </constructor-arg>
    </bean>

All other behaviour is the same.

## License

Apache License 2.0: See LICENCE file for details.
